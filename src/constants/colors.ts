export const COLORS = {
    main: '#003eff',
    notActive: '#c2c2c2',
    text: {
        light: '#fff',
        gray: '#8e8e8f',
        dark: '#1e1d20'
    },
    accent: '#d0021b',
    footer: '#f4f4f4',
};
