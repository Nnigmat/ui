import { Button } from './components/button';
import { TextInput, TextArea } from './components/Inputs';
import { OrganizationInfo } from './components/organizationInfo';
import { Typography } from './components/typography';
import { PersonVertical, PersonHorizontal } from './components/person';
import { PageLoader } from './components/page-loader';
import { ImgLink } from './components/img-link'
import { Footer } from './components/footer'

export {
    Button,
    OrganizationInfo,
    Typography,
    PersonVertical,
    PersonHorizontal,
    PageLoader,
    TextInput,
    TextArea,
    ImgLink,
    Footer
};
