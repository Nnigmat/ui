import avaFemale from './ava-fem.svg';
import avaMale from './ava-mal.svg';
import udsIcon from './uds-info.svg';
import logoGray from './logo-gray.svg';
import phoneNumber from './number.svg'

// Social nets icons
import vkIcon from './vk.svg';
import okIcon from './ok.svg';
import twitterIcon from './twitter.svg';
import instagramIcon from './instagram.svg';
import facebookIcon from './facebook.svg';

export {
    avaMale,
    avaFemale,
    udsIcon,
    logoGray,
    phoneNumber,
    vkIcon,
    okIcon,
    twitterIcon,
    instagramIcon,
    facebookIcon
};
