import styled from 'styled-components';

import { COLORS } from '../../constants/colors';

export const StyledDynamicTypography = styled.div`
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap&subset=cyrillic');
  
  margin: 0;
  padding: 0;
  font-family: Montserrat, sans-serif;
  color: ${COLORS.text.dark};
  display: inline;
  
  ${props => props.as === 'h1' && h1Style}
  ${props => props.as === 'h2' && h2Style}
  ${props => props.as === 'h3' && h3Style}
  ${props => props.as === 'p' && pStyle}
  ${props => props.variant === 'description' && descriptionStyle}
  ${props => props.variant === 'primary' && primaryStyle}
  
  ${props => props.fontSize && 'font-size: ' + props.fontSize + ';'}
  ${props => props.color && 'color: ' + props.color + ';'}
  ${props => props.textTransform && 'text-transform: ' + props.textTransform + ';'}
  ${props => props.margin && 'margin: ' + props.margin + ';'}
  ${props => props.padding && 'padding: ' + props.padding + ';'}
  ${props => props.opacity && 'opacity: ' + props.opacity + ';'}
  ${props => props.fontWeight && 'font-weight: ' + props.fontWeight + ';'}
  ${props => props.customStyle && props.customStyle}
`;

const h1Style = `
  display: block;
  font-weight: 700;
  font-size: 50px;
`;

const h2Style = `
  display: block;
  font-weight: 600;
  font-size: 38px;
`;

const h3Style = `
  display: block;
  font-weight: 600;
  font-size: 22px;
`;

const pStyle = `
  display: block;
  font-weight: 400;
  font-size: 16px;
`;

const descriptionStyle = `
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
  opacity: 0.5;
`;

const primaryStyle = `
  color: ${COLORS.main};
`;
