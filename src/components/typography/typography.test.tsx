import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { Typography } from './';

describe('Typography component', () => {
  it('Отрисовка стандартного компонента', () => {
    const typography = renderer.create(<Typography />).toJSON();

    expect(typography).toMatchSnapshot();
  });

  it('Html компонент заменяется на указанный в props.component', () => {
    const customComponentTypography = renderer.create(<Typography component="h3" />).toJSON();

    expect(customComponentTypography).toHaveProperty('type', 'h3');
    expect(customComponentTypography).toMatchSnapshot();
  });

  it('Html компонент заменяется на указанный в таблице variant', () => {
    const customComponentTypography = renderer.create(<Typography variant="description" />).toJSON();

    expect(customComponentTypography).toHaveProperty('type', 'span');
    expect(customComponentTypography).toMatchSnapshot();
  });

  it('Принимает параметры для стилизации', () => {
    const styledColorTypography = renderer.create(<Typography color="#fefefe" />).toJSON();
    const styledMarginTypography = renderer.create(<Typography margin="10px" />).toJSON();
    const styledPaddingTypography = renderer.create(<Typography padding="10px" />).toJSON();
    const styledFontSizeTypography = renderer.create(<Typography fontSize="10px" />).toJSON();
    const styledTextTransformTypography = renderer.create(<Typography textTransform="uppercase" />).toJSON();
    const styledFontWeightTypography = renderer.create(<Typography fontWeight={700} />).toJSON();
    const styledOpacityTypography = renderer.create(<Typography opacity="0.3" />).toJSON();

    expect(styledColorTypography).toMatchSnapshot();
    expect(styledMarginTypography).toMatchSnapshot();
    expect(styledPaddingTypography).toMatchSnapshot();
    expect(styledFontSizeTypography).toMatchSnapshot();
    expect(styledTextTransformTypography).toMatchSnapshot();
    expect(styledFontWeightTypography).toMatchSnapshot();
    expect(styledOpacityTypography).toMatchSnapshot();
  });

  it('Захватывает стили для опреденных компонентов, предустановленных вариаций', () => {
    const h1Typography = renderer.create(<Typography component="h1" />).toJSON();
    const h2Typography = renderer.create(<Typography component="h2" />).toJSON();
    const h3Typography = renderer.create(<Typography component="h3" />).toJSON();
    const pTypography = renderer.create(<Typography component="p" />).toJSON();
    const primaryTypography = renderer.create(<Typography variant="primary" />).toJSON();
    const secondaryTypography = renderer.create(<Typography variant="description" />).toJSON();

    expect(h1Typography).toMatchSnapshot();
    expect(h2Typography).toMatchSnapshot();
    expect(h3Typography).toMatchSnapshot();
    expect(pTypography).toMatchSnapshot();
    expect(primaryTypography).toMatchSnapshot();
    expect(secondaryTypography).toMatchSnapshot();
  });

  it('Кнопка как typography', () => {
    const jestCallback = jest.fn();
    const btnTypography = shallow(<Typography component={'button'} onClick={jestCallback} />);

    btnTypography.simulate('click');
    btnTypography.simulate('click');

    expect(jestCallback.mock.calls.length).toBe(2);
  });
});
