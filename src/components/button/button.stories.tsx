import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Button } from '.';

storiesOf('Button', module)
    .add('with text', () => <Button onClick={action('button-click')}>Кнопка записаться</Button>)
    .add('disabled', () => <Button disabled={true} onClick={action('button-click')}>Кнопка записаться</Button>)
    .add('new disabled', () => <Button buttonType='disabled' onClick={action('button-click')}>Кнопка записаться</Button>)
    .add('secondary', () => <Button buttonType='secondary' onClick={action('button-click')}>Кнопка записаться</Button>)