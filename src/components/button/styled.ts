import styled from 'styled-components';

import { COLORS } from '../../constants/colors';

// Get background color based on button type and disabled param
const bgColor = (buttonType: string, isDisabled: boolean) => {
    if (isDisabled) {
        return COLORS.notActive;
    }

    switch (buttonType) {
        case 'primary':
            return COLORS.main;
        case 'secondary':
            return COLORS.text.light;
        case 'disabled':
            return COLORS.notActive;
        default:
            return COLORS.main;
    }
}

export const ButtonWrapper = styled.button`
    @media screen and (min-width: 810px) {
        width: auto;
    }

    width: 100%;
    border-radius: 28px;
    background-color: ${(props: { buttonType: string, disabled?: boolean }) => bgColor(props.buttonType, props.disabled)};
    padding: 13px 34px;
    color: ${(props: { buttonType: string }) => props.buttonType === 'secondary' ? COLORS.text.dark : COLORS.text.light};
    font-size: 18px;
    font-weight: 800;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    outline: none;
    cursor: ${(props: { buttonType: string, disabled?: boolean }) => props.buttonType === 'disabled' || props.disabled ? 'not-allowed' : 'pointer'};
    border: none;
`;
