import styled from 'styled-components'

import { COLORS } from '../../constants/colors'

export const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: column wrap;
    
    justify-content: center;
    align-items: center;
    text-align: center;

    background-color: ${COLORS.main};
    margin: 0;
    
    padding: 0 20%;
    height: 200px;
    border-radius: 0;

    @media (min-width: 810px) {
        padding: 0 0;
        height: 236px;
        border-radius: 16px;
        margin: ${(props: { margin?: string }) => props.margin ? props.margin : 0};
    }
`