import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { ReachGoals } from '.';

storiesOf('ReachGoals', module)
    .add('default', () => (
        <ReachGoals
            title='Достигайте своих целей'
            buttonText='Записаться'
            onClick={action('button-click')}
        />
    ))
    .add('margined', () => (
        <ReachGoals
            title='Достигайте своих целей'
            buttonText='Записаться'
            onClick={action('button-click')}
            margin='0 10%'
        />
    ))