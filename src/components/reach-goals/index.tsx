import React from 'react'

import { Wrapper } from './styled'
import { Typography } from '../typography'
import { Button } from '../button'
import { COLORS } from '../../constants/colors'

interface OnClickType {
    (event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void;
}

/**
 * @prop {string} title The title of a block
 * @prop {string} buttonText The text in button
 * @prop {OnClickType} onClick The function that button calls. Optional parameter
 * @prop {string} margin The blocks margin. Optional parameter
 */
interface ReachGoalsProps {
    title: string;
    buttonText: string;
    onClick?: OnClickType;
    margin?: string;
}

/**
 * The ReachGoals component renders the text and button
 *  
 * Wrapper:
 *     Title
 *     Button
 */
export const ReachGoals: React.FC<ReachGoalsProps> = ({ title, buttonText, onClick, margin }) => {
    const titleFontSize = `
        font-size: 22px;

        @media (min-width: 810px) {
            font-size: 50px;
        } 
    `

    return (
        <Wrapper margin={margin}>
            <Typography
                component={'h1'}
                color={COLORS.text.light}
                customStyle={titleFontSize}
                fontWeight={700}
                margin='0 0 24px 0'
            >
                {title}
            </Typography>
            <Button
                onClick={onClick}
                buttonType='secondary'
            >
                <Typography fontSize='16px'>
                    {buttonText}
                </Typography>
            </Button>
        </Wrapper>)
}