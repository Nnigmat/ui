import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { ReachGoals } from './';

describe('ReachGoals component', () => {
    it('Default rendering', () => {
        const tree = renderer.create(
            <ReachGoals
                title='Достигайте своих целей'
                buttonText='Записаться'
            />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
})