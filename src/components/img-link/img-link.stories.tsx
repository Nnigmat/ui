import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { ImgLink } from '.';
import { instagramIcon } from '../../assets/'

storiesOf('Image link', module)
    .add('default icon open on current page', () => (
        <ImgLink
            href='https://instagram.com'
            src={instagramIcon}
            stay={true}
        />
    ))
    .add('default icon open on new tab', () => (
        <ImgLink
            href='https://instagram.com'
            src={instagramIcon}
        />
    ))
    .add('styled icon', () => (
        <ImgLink
            href='https://instagram.com'
            src={instagramIcon}
            style={{ height: '50px' }}
        />
    ))

