import React from 'react'

import { Link } from './styled'

/**
 * @prop {string} src Link to the image
 * @prop {string} href Hyper reference to the target
 * @prop {boolean} stay Open link on the current page or at the new tab
 */
interface ImgLinkProps extends React.HTMLProps<HTMLImageElement> {
    src: string;
    href: string;
    stay?: boolean;
}

/**
 * The component that create the link out of image.
 * 
 * Basically, output: <a><img /></a>.
 */
export const ImgLink: React.FC<ImgLinkProps> = ({ src, href, stay, style }) => (
    <>
        {stay
            ?
            <Link href={href}>
                <img src={src} style={style} />
            </Link>
            :
            <Link href={href} target='_blank' rel='noopener noreferrer'>
                <img src={src} style={style} />
            </Link>
        }

    </>
)

ImgLink.defaultProps = {
    stay: false
}