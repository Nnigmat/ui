import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

import { ImgLink } from '.'
import { instagramIcon } from '../../assets'

describe('ImageLink component', () => {
    it('basic rendering', () => {
        const tree = renderer.create(
            <ImgLink
                src={instagramIcon}
                href='https://instagram.com'
            />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
})


