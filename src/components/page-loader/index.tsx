import * as React from 'react';

import { DualRing, Wrapper } from './styled';

export const PageLoader = () => (
    <Wrapper>
        <DualRing />
    </Wrapper>
);
