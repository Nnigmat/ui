import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { PageLoader } from '.';

storiesOf('page loader', module)
    .add('loader', () => <PageLoader />);
