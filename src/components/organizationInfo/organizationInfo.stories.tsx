import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { OrganizationInfo } from '.';
import { udsIcon } from '../../assets/'

storiesOf('OrganizationInfo', module)
    .add('with text and non svg image', () => (
        <OrganizationInfo bottomText="г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4"
            buttonText="Записаться"
            mainText="С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. "
            src="https://im0-tub-ru.yandex.net/i?id=655bb4a1ed3844a3a24da34dee59ebd2&n=13&exp=1"
            title="ГБУ ЦДиК «Южное Бутово»"
            onClick={action('button-click')}
        />
    ))
    .add('with text and svg image', () => (
        <OrganizationInfo bottomText="г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4"
            buttonText="Записаться"
            mainText="С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. Наш Центр является многофункциональным и многопрофильным досуговым учреждением города Москвы: мы поддерживаем и развиваем все виды творчества, досуговой деятельности и любительского спорта. У Центра есть целых 9 отделений, поэтому до нас удобно добираться любому жителю Южного Бутова."
            src={udsIcon}
            title="ГБУ ЦДиК «Южное Бутово»"
            onClick={action('button-click')}
        />
    ));
