import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { OrganizationInfo } from './';
import { udsIcon } from '../../assets'

describe('OrganizationInfo component', () => {
    it('отрисовывание с картинкой', () => {
        const tree = renderer.create(
            <OrganizationInfo
                bottomText={'г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4'}
                buttonText={'Записаться'}
                mainText={'С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. '}
                src={'https://im0-tub-ru.yandex.net/i?id=655bb4a1ed3844a3a24da34dee59ebd2&n=13&exp=1'}
                title={'ГБУ ЦДиК «Южное Бутово»'}
            />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('отрисовывание с svg', () => {
        const tree = renderer.create(
            <OrganizationInfo
                bottomText={'г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4'}
                buttonText={'Записаться'}
                mainText={'С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. '}
                src={udsIcon}
                title={'ГБУ ЦДиК «Южное Бутово»'}
            />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('Запускает обработчик при нажатии', () => {
        const jestCallback = jest.fn();

        const component = shallow((
            <OrganizationInfo
                bottomText={'г.Москва, ЮЗАО, ул. Скобелевская, д. 23, к. 4'}
                buttonText={'Записаться'}
                mainText={'С 2006 года базовый центр района по организации досуга и спортивных занятий для жителей. '}
                src={'https://im0-tub-ru.yandex.net/i?id=655bb4a1ed3844a3a24da34dee59ebd2&n=13&exp=1'}
                title={'ГБУ ЦДиК «Южное Бутово»'}
                onClick={jestCallback}
            />
        )
        );

        component.find('Button').simulate('click');

        expect(jestCallback.mock.calls.length).toBe(1);
    });
});
