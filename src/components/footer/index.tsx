import React from 'react';

import {
    Main,
    LeftSection,
    RightSection,
    PhoneNumber,
    SocialNetworks,
} from './styled'

import {
    logoGray,
    phoneNumber,

    vkIcon,
    okIcon,
    instagramIcon,
    facebookIcon,
    twitterIcon
} from '../../assets'

import { ImgLink } from '../img-link'

/**
 * Footer component
 * Consists of two parts:
 *     LeftSection:
 *         Logo (Image)
 *     RightSection:
 *         Phone number (Image)
 *         Links to social networks (Icons) - disappears at low resolution.
 */
export const Footer = () => (
    <Main>
        <LeftSection>
            <ImgLink
                src={logoGray}
                href='/'
                stay={true}
                style={{ height: '32px' }}
            />
        </LeftSection>
        <RightSection>
            <PhoneNumber
                src={phoneNumber}
                style={{ height: '24px' }}
            />

            <SocialNetworks>
                <ImgLink src={vkIcon} href='https://vk.com' />
                <ImgLink src={instagramIcon} href='https://instagram.com' />
                <ImgLink src={okIcon} href='https://ok.ru' />
                <ImgLink src={facebookIcon} href='https://facebook.com' />
                <ImgLink src={twitterIcon} href='https://twitter.com' />
            </SocialNetworks>
        </RightSection>
    </Main>
)