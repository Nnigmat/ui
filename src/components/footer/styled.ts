import styled from 'styled-components';

import { COLORS } from '../../constants/colors'

const Wrapper = styled.div`
    display: -webkit-flex;
    display: flex;
    -webkit-flex-flow: row wrap;
    flex-flow: row wrap;
`

export const Main = styled(Wrapper)`
    background-color: ${COLORS.footer};
    padding: 24px 6% 32px 6%;
    align-content: center;
    max-height: 90px;
`

export const LeftSection = styled(Wrapper)`
    flex: 0 0 100%;
    justify-content: center;

    @media (min-width: 810px) {
        flex: 0 0 30%;
        justify-content: flex-start;
    }
`

export const RightSection = styled(Wrapper)`
    flex: 0 0 100%;
    justify-content: center;

    @media (min-width: 810px) {
        flex: 1 0 50%;
        justify-content: flex-end;
    }
`

export const PhoneNumber = styled.img` 
    display: flex;
    flex: 0 0 100%;

    @media (min-width: 810px) {
        flex: 0 0 30%;
        margin-right: 64px;
    }
`

export const SocialNetworks = styled(Wrapper)`
    display: none;

    @media (min-width: 810px) {
        display: flex;
        justify-content: flex-end;
    }

    & * {
        margin: 0px 16px 0px 0px;
    }
`