import * as React from 'react';
import renderer from 'react-test-renderer';
import { shallow, configure, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

import { Footer } from '.'

describe('Footer component', () => {
    it('basic rendering', () => {
        const tree = renderer.create(
            <Footer />
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
})

