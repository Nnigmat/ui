import styled from 'styled-components';

export const PersonContainerColumn = styled.div`
    padding: 15px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: 300px;
`;

export const PersonContainerRow = styled.div`
    padding: 15px;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: flex-start;
    width: 900px;
`;

export const PersonAvatarContainer = styled.div`
    height: 100px;
    width: 100px;
    margin-bottom: 20px;
    flex-shrink: 0;
`;

export const CenterText = styled.span`
    text-align: center;
`;

export const PersonName = styled.span`
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 5px;
`;

export const PersonJobTitle =  styled.span`
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 10px;
`;

export const PersonJobDescription =  styled.span`
        font-size: 14px;
`;

export const PersonColumnDescriptionContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    margin-left: 20px;
`;

export const PersonImg = styled.img`
    height: 100%;
    width: 100%;
    border-radius: 50%;
`;