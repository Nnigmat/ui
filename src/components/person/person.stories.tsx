import * as React from 'react';
import { storiesOf } from '@storybook/react';

import { PersonVertical, PersonHorizontal } from '.';
import { exampleAva } from '../../assets';

const des = `Инструктор лечебной физкультуры Национального государственного университета физической культуры,
    спорта и здоровья (ГДОИФК) им. П. Ф. Лесгафта. Инструктор-методист по оздоровительным формам физической
    культуры Национальный государственный университет физической культуры, спорта и здоровья (ГДОИФК) им. П. Ф. Лесгафта.`;
const name = 'Тестович Тест Тестов';
const title = 'Занятия: Раннее развитие с элементами айкидо для детей 5 – 17 лет.';

storiesOf('Person', module)
    .add('horizontal', () => <PersonHorizontal name={name} jobDescription={des} jobTitle={title} gender="male" />)
    .add('vertical', () => <PersonVertical name={name} jobTitle={title} gender="female" />)
    .add('with ava', () => <PersonHorizontal image={exampleAva} name={name} jobTitle={title} gender="female" />);