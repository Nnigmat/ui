import * as React from 'react';

import { avaMale, avaFemale } from '../../assets';

import {
    PersonContainerColumn, PersonContainerRow, PersonAvatarContainer, CenterText, PersonName, PersonJobTitle,
    PersonJobDescription, PersonColumnDescriptionContainer, PersonImg
} from './styled';

interface PersonProps {
    name: string;
    jobTitle: string;
    gender: 'female' | 'male';
    image?: string;
    jobDescription?: string;
}

const DefaultImage = {
    female: avaFemale,
    male: avaMale
};

export const PersonVertical: React.FunctionComponent<PersonProps> = ({ image, name, jobTitle, gender }) => (
    <PersonContainerColumn>
        <PersonAvatarContainer>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainer>
        <PersonJobTitle>
            <CenterText>
                {name}
            </CenterText>
        </PersonJobTitle>
        <PersonJobDescription>
            <CenterText>
                {jobTitle}
            </CenterText>
        </PersonJobDescription>
    </PersonContainerColumn>
);

export const PersonHorizontal: React.FunctionComponent<PersonProps> = ({image, name, jobTitle, jobDescription, gender}) => (
    <PersonContainerRow>
        <PersonAvatarContainer>
            <PersonImg
                src={image || DefaultImage[gender]}
            />
        </PersonAvatarContainer>
        <PersonColumnDescriptionContainer>
            <PersonName>
                {name}
            </PersonName>
            <PersonJobTitle>
                {jobTitle}
            </PersonJobTitle>
            <PersonJobDescription>
                {jobDescription}
            </PersonJobDescription>
        </PersonColumnDescriptionContainer>
    </PersonContainerRow>
);