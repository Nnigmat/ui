import * as React from 'react';
import renderer from 'react-test-renderer';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { TextInput, TextArea } from '..';

describe('Inputs components', () => {
    it('Отрисовываютсяи без параметров', () => {
        const textInput = renderer.create(<TextInput />).toJSON();
        const textArea = renderer.create(<TextArea />).toJSON();

        expect(textInput).toMatchSnapshot();
        expect(textArea).toMatchSnapshot();
    });

    it('Отрисовка с label', () => {
        const textInput = renderer.create(<TextInput label="Очень нужный заголовок" id="test-id-1" />).toJSON();
        const textArea = renderer.create(<TextArea label="Теперь понятно что сюда вводить" id="test-id-2" />).toJSON();

        expect(textInput).toMatchSnapshot();
        expect(textArea).toMatchSnapshot();
    });

    it('Отрисовка с required label', () => {
        const textInput = renderer.create(<TextInput required label="Очень нужный заголовок" id="test-id-1" />).toJSON();
        const textArea = renderer.create(<TextArea required label="Теперь понятно что сюда вводить" id="test-id-2" />).toJSON();

        expect(textInput).toMatchSnapshot();
        expect(textArea).toMatchSnapshot();
    });

    it('Запускает обработчик при нажатии', () => {
        const jestCallback = jest.fn();
        let value = '';
        let handleChange = (val) => {
            value = val;
            jestCallback();
        };

        const input = mount(
            <TextInput id="test-id" value={value} onChange={handleChange}/>
        );

        input
            .find('input#test-id')
            .simulate('change', { target: { value: 'H' } })
            .simulate('change', { target: { value: 'He' } })
            .simulate('change', { target: { value: 'Hel' } })
            .simulate('change', { target: { value: 'Hell' } })
            .simulate('change', { target: { value: 'Hello' } });

        expect(jestCallback.mock.calls.length).toBe(5);
        expect(value).toBe('Hello');
    });

    it('Не падает без обработчика', () => {
        const textInput = mount(
            <TextInput id="test-id" value="" />
        );

        textInput
            .find('input#test-id')
            .simulate('change', { target: { value: 'some value' } });

        expect(textInput).toMatchSnapshot();
    });
});
