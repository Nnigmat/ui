import * as React from 'react';

import { Label, StyledInput, Wrapper } from '../common/styled';

/**
 * @prop {Function} [onChange] Обработчик изменения значения инпута.
 * @prop {React.Ref<HTMLInputElement>} [innerRef] Ссылка на Dom элемент.
 */
interface TextInputProps extends Omit<React.HTMLProps<HTMLInputElement>, 'onChange'> {
    onChange?: (value: string) => void;
    innerRef?: React.Ref<HTMLInputElement>;
}

// Компонент - текстовое поле ввода.
const Input = ({ id, label, className, required, type = 'text', children, onChange, innerRef, ...rest }: TextInputProps) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;

        if (onChange) {
            onChange(value);
        }
    };

    return (
        <Wrapper className={className}>
            <Label htmlFor={id} required={required}>{label}</Label>
            <StyledInput ref={innerRef} id={id} {...rest} type={type} required={required} onChange={handleChange} />
        </Wrapper>
    );
};

const TextInput: React.ForwardRefExoticComponent<TextInputProps> = React.forwardRef((props, ref) =>
    <Input innerRef={ref} {...props} />);

export default  TextInput;
