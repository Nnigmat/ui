const { readFileSync, writeFileSync } = require('fs')
const path = require('path')

const { globSvg } = require('./utils')

const generateUniqueIdsSvg = (svg, uniqueIdPrefix) => svg
// удаляем префиксы
    .replace(new RegExp(`${uniqueIdPrefix}-`, 'gi'), '')
    // удаляем префиксы
    .replace(new RegExp(`${uniqueIdPrefix}-`, 'gi'), '')
    // унифицируем id
    .replace(/id="(.+)"/gi, `id="${uniqueIdPrefix}-$1"`)
    // унифицируем ссылки вставки
    .replace(/xlink:href="#(.+)"/gi, `xlink:href="#${uniqueIdPrefix}-$1"`)
    // унифицируем маски по ссылкам
    .replace(/mask="url\(#(.+)\)"/gi, `mask="url(#${uniqueIdPrefix}-$1)"`)
    // унифицируем заливку по ссылкам
    .replace(/fill="url\(#(.+)\)"/gi, `fill="url(#${uniqueIdPrefix}-$1)"`)

module.exports = (iconSetName) => {
    globSvg(iconSetName).forEach((currentPath) => {

        const prevSvg = readFileSync(currentPath, 'utf-8')

        const { name } = path.parse(currentPath)

        const uniqueIdsSvg = generateUniqueIdsSvg(prevSvg, `${iconSetName}-${name}`)

        writeFileSync(currentPath, uniqueIdsSvg, 'utf-8')
    })
}
